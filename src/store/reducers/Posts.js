import { ADD_POST, ADD_COMMENT } from '../actions/ActionTypes'

const initialState = {
    posts: [{
        id: Math.random(),
        nickname: 'Testador',
        email: 'testador@teste.com',
        image: require('../../../assets/imgs/bw.jpg'),
        comments: [{
            nickname: 'teste 01',
            comment: 'Comentario 01'
        },{
            nickname: 'teste 01',
            comment: 'Comentario 01'
        }]
    }, {
        id: Math.random(),
        nickname: 'testando',
        email: 'teste@teste.com',
        image: require('../../../assets/imgs/bw.jpg'),
        comments: []
    }]
}

const reducer = (state = initialState, action) => {
    switch(action) {
        case ADD_POST:
            return {
                ...state,
                posts: state.posts.concat({
                    ...action.payload
                })
            }
        case ADD_COMMENT:
            return {
                ...state,
                posts: state.posts.map(post => {
                    if (post.id === action.payload.postId) {
                        if (post.comments) {
                            post.comments = post.comments.concat(
                                action.payload.comment
                            )
                        } else {
                            post.comments = [action.payload.comment]
                        }
                    }
                    return post
                })
            }
        default:
            return state
    }
}

export default reducer